from django.db import migrations, models

def forward(apps, schema_editor):
    Service = apps.get_model('veterans', 'Service')
    Conflict = apps.get_model('veterans', 'Conflict')
    db = schema_editor.connection.alias
    Service.objects.using(db).bulk_create([
        Service(name='U.S. Army'),
        Service(name='U.S. Marine Corps'),
        Service(name='U.S. Navy'),
        Service(name='U.S. Air Force'),
        Service(name='U.S. Coast Guard'),
        ])

    Conflict.objects.using(db).bulk_create([
        Conflict(name='Revolutionary War'),
        Conflict(name='War of 1812'),
        Conflict(name='Mexican-American War'),
        Conflict(name='Civil War'),
        Conflict(name='World War I'),
        Conflict(name='World War II'),
        Conflict(name='Korean Conflict'),
        Conflict(name='Vietnam War'),
       ])

def reverse(apps, schema_editor):
    MonumentType = apps.get_model('monuments', 'MonumentType')
    db = schema_editor.connection.alias
    MonumentType.objects.using(db).all().delete()

class Migration(migrations.Migration):
    dependencies = [
        ('veterans', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forward, reverse),
    ]
