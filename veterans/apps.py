from django.apps import AppConfig


class VeteransConfig(AppConfig):
    name = 'veterans'
