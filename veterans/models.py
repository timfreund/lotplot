from django.db import models
from simple_history.models import HistoricalRecords

from interments.models import Interment

class Service(models.Model):
    name = models.CharField(unique=True, max_length=255)

class Conflict(models.Model):
    name = models.CharField(unique=True, max_length=255)

class ServiceRecord(models.Model):
    interment = models.ForeignKey(Interment, related_name='service_records', on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.PROTECT)
    conflict = models.ForeignKey(Conflict, null=True, on_delete=models.PROTECT)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    unit = models.CharField(max_length=255, blank=True)
    last_rank = models.CharField(max_length=255, blank=True)
    notes = models.TextField(blank=True)

    history = HistoricalRecords()
