from django.conf.urls import url
from django.urls import path

from . import views

app_name = "donations"

urlpatterns = [
    url(regex=r"^one_time$",
        view=views.OneTimeDonationView.as_view(),
        name="one_time",
    ),
    url(regex=r"^one_time_checkout$",
        view=views.OneTimeCheckoutView.as_view(),
        name="one_time_checkout",
    ),
    url(regex=r"^one_time_confirmation$",
        view=views.one_time_confirmation_view,
        name="one_time_confirmation",
    ),
]
