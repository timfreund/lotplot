import uuid

from django.conf import settings
from django.db import models

class Donation(models.Model):
    guid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    contributor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    contributor_statement = models.TextField(blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    stripe_token = models.CharField(max_length=28, blank=True)
    stripe_capture_attempts = models.IntegerField(default=0)
    stripe_capture_success = models.BooleanField(default=False)

    @property
    def amount_in_cents(self):
        return self.amount * 100
