import logging

from django.core.mail import send_mail
from django import forms

from lotplot.users.models import User
from .models import Donation

log = logging.getLogger(__name__)

class OneTimeDonationForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()

    address1 = forms.CharField()
    address2 = forms.CharField(required=False)
    city = forms.CharField()
    state = forms.CharField()
    postal_code = forms.CharField()

    amount = forms.DecimalField(min_value=1.0, decimal_places=2)
    purpose = forms.CharField()

    def create_donation(self):
        user = self.get_or_create_user()
        donation = Donation()
        donation.contributor = user
        donation.amount = self.cleaned_data['amount']
        donation.contributor_statement = self.cleaned_data['purpose']
        donation.save()
        return donation

    def get_or_create_user(self):
        print(self.cleaned_data)
        email = self.cleaned_data['email']
        user, user_created = User.objects.get_or_create(email=email)
        if user_created:
            user.name = self.cleaned_data['name']
            user.address1 = self.cleaned_data['address1']
            user.address2 = self.cleaned_data['address2']
            user.city = self.cleaned_data['city']
            user.state = self.cleaned_data['state']
            user.postal_code = self.cleaned_data['postal_code']
        else:
            log.error("TODO: Existing user, should validate before we overwrite")
            user.name = self.cleaned_data['name']
            user.address1 = self.cleaned_data['address1']
            user.address2 = self.cleaned_data['address2']
            user.city = self.cleaned_data['city']
            user.state = self.cleaned_data['state']
            user.postal_code = self.cleaned_data['postal_code']

        user.save()
        send_mail("User account updated",
                  "Thanks for updating your account.  If you didn't update your account, you should let us know",
                  "lotplot@lancastercemetery.org",
                  [user.email],
                  fail_silently=False,
                  )

        return user
