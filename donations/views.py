import uuid

from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from .forms import OneTimeDonationForm
from .models import Donation

# Create your views here.
class OneTimeDonationView(FormView):
    template_name = "donations/one_time_donation.html"
    form_class = OneTimeDonationForm
    success_url = '/donations/one_time_checkout'

    def form_valid(self, form):
        donation = form.create_donation()
        self.request.session['donation_guid'] = str(donation.guid)
        return super().form_valid(form)

class OneTimeCheckoutView(TemplateView):
    template_name = "donations/one_time_checkout.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("found donation_guid %s" % self.request.session['donation_guid'])
        donation = Donation.objects.filter(guid=uuid.UUID(self.request.session['donation_guid'])).first()
        # donation = Donation.objects.find(guid=self.request.session['donation_guid'])
        context['donation'] = donation
        context['stripe_key'] = settings.PINAX_STRIPE_PUBLIC_KEY
        return context

@csrf_exempt
def one_time_confirmation_view(request, **kwargs):
    print(request.POST)

    ### Sample Payload
    # {'order_id': ['a08bd400-9cd3-4293-ad04-ebe7377f0fc2'],
    #  'stripeToken': ['tok_1DGB7iFpHNcuXIYMUCpSVMGj'],
    #  'stripeTokenType': ['card'],
    #  'stripeEmail': ['user@example.org'],
    #  'stripeBillingName': ['Some User'],
    #  'stripeBillingAddressCountry': ['United States'],
    #  'stripeBillingAddressCountryCode': ['US'],
    #  'stripeBillingAddressZip': ['17602-1234'],
    #  'stripeBillingAddressLine1': ['1234 Main Street'],
    #  'stripeBillingAddressCity': ['Lancaster'],
    #  'stripeBillingAddressState': ['PA']}

    return render(request, "donations/one_time_confirmation.html", {})
