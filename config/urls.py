from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from rest_framework import routers as restrouters
from rest_framework.schemas import get_schema_view

from interments.restviews import *
from lots.restviews import *
from monuments.restviews import *

restrouter = restrouters.DefaultRouter()
restrouter.register(r'interments', IntermentViewSet)
restrouter.register(r'intermenttypes', IntermentTypeViewSet)
restrouter.register(r'lots', LotViewSet)
restrouter.register(r'maps', MapViewSet)
restrouter.register(r'mapcells', MapCellViewSet)
restrouter.register(r'monuments', MonumentViewSet)

urlpatterns = [
    path("", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    path(
        "about/",
        TemplateView.as_view(template_name="pages/about.html"),
        name="about",
    ),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path(
        "users/",
        include("lotplot.users.urls", namespace="users"),
    ),
    path("accounts/", include("allauth.urls")),
    # Your stuff: custom urls includes go here
    path('api/', include(restrouter.urls)),
    path('api/interments/<int:id>/document-upload', IntermentDocumentUpload.as_view()),
    path('api/interments/<int:id>/image-upload', IntermentImageUpload.as_view()),
    path('api/monuments/<int:id>/document-upload', MonumentDocumentUpload.as_view()),
    path('api/monuments/<int:id>/image-upload', MonumentImageUpload.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-schema/', get_schema_view(
        title='lotplot',
        description='lotplot-api',
        version='0.0.1',
        public=True,
    ), name='openapi-schema'),
    path('donations/', include('donations.urls')),
    path('interments/', include('interments.urls')),
    path('lots/', include('lots.urls')),
    path('monuments/', include('monuments.urls')),
    path("payments/", include("pinax.stripe.urls")),
] + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
