#!/usr/bin/env bash

SRC=./lotplot/static/js/elm

set -x
for FILENAME in `find ${SRC} -name \*.elm`;
do
    echo ${FILENAME}
    elm make ${FILENAME} --output=${FILENAME}.js
done;

target_user=`stat -c '%u' elm.json`
chown -R ${target_user} elm-stuff lotplot/static/js/elm
