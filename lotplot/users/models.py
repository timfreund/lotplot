from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = CharField(_("Name of User"), blank=True, max_length=255)
    address1 = CharField(_("Address 1"), blank=True, max_length=255)
    address2 = CharField(_("Address 2"), blank=True, max_length=255)
    city = CharField(_("City"), blank=True, max_length=128)
    state = CharField(_("State"), blank=True, max_length=64)
    postal_code = CharField(_("ZIP Code"), blank=True, max_length=16)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})
