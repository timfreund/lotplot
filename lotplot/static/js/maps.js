var d = null;

function click(target){
    console.log(target);
}

function initializeMap(svgUrl){
    console.log("initializeMap");
    d3.xml(svgUrl)
        .then(data => {
            var baseurl = svgUrl.substring(0, svgUrl.lastIndexOf("/") + 1);
            var mapsvg = data.documentElement;
            mapsvg.querySelectorAll("image").forEach((element) => {
                element.href.baseVal = baseurl + element.href.baseVal;
            });

            d3.select('div#map').node().append(mapsvg);
            d3.select("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", "0 0 2450 1840")
                .classed("svg-content-responsive", true);
        });
}

function requestLotHighlight(){
    var lotName = document.getElementById("lotinput").value;
    $.ajax({dataType: 'json',
            url: '/api/mapcells/by-lot/?lot_name=' + lotName,
            success: function (data) {
                console.log(data);
                console.log(data[0]);
                console.log(data[0].cell_id);
                d = data;
                data.forEach(mapcell => svgAddStyle(mapcell.cell_id, "highlight"));
            }});
    console.log(lotName);
}

function svgAddStyle(elementId, className){
    d3.select("#" + elementId).classed(className, true);
}
