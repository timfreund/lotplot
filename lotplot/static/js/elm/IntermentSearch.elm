module IntermentSearch exposing (main)

import Browser
import Debug exposing (log, toString)
import Html exposing (..)
import Html.Attributes exposing (class, href, id, placeholder, type_, value)
import Html.Events exposing (onInput)
import Http
import Json.Decode exposing (Decoder, at, bool, field, int, list, map2, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)
import List

type alias Interment =
    { id : Int
    , intermentDate : String
    , disintermentDate : String
    , deathDate : String
    , birthDate : String
    , name: String
    , familyName: String
    , notes: String
    , lot: String
    }

intermentDecoder : Decoder Interment
intermentDecoder =
    -- note that this must go in the order of
    -- the fields as defined in the type alias
    -- ... I wonder if there's a way to map
    -- field to field based on name rather than
    -- order
    succeed Interment
        |> required "id" int
        |> optional "interment_date" string ""
        |> optional "disinterment_date" string ""
        |> optional "death_date" string ""
        |> optional "birth_date" string ""
        |> required "name" string
        |> optional "family_name" string ""
        |> optional "notes" string ""
        |> optional "lot" string ""

listOfIntermentsDecoder : Decoder (List Interment)
listOfIntermentsDecoder =
    list intermentDecoder

type Interments
    = Loading
    | Loaded (List Interment)
    | Errored String

type alias Model =
    { searchTerm : String
    , interments : Interments
    }

type Msg
    = SearchResults (Result Http.Error (List Interment))
    | SearchTerm String

getSearchResults : String -> Cmd Msg
getSearchResults searchTerm =
    Http.request
        { method = "GET"
        , headers = [ Http.header "content-type" "application/json; charset=UTF-8" ]
        , timeout = Nothing
        , tracker = Nothing
        , url = "/api/interments/trigram-search/?term=" ++ searchTerm
        , expect = Http.expectJson SearchResults listOfIntermentsDecoder --(list intermentDecoder)
        , body = Http.emptyBody
        }

resultRow : Interment -> Html msg
resultRow interment =
    log "hi"
    tr [ ] [
         td [] [ text interment.familyName ]
        , td [] [ text interment.name ]
        , td [] [ text interment.deathDate ]
        , td [] [
              a [ href ("/interments/interment/" ++ (String.fromInt interment.id)) ] [ text "View" ]
             ]
        ]

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

update : Msg -> Model -> (Model, Cmd Msg )
update msg model =
    case msg of
        SearchResults (Ok interments) ->
            log "ok2"
            log (String.fromInt (List.length interments))
            ({ model | interments = Loaded interments }, Cmd.none)
        SearchResults (Err httpError) ->
            log "err"
            -- note that toString won't work in prod:
            -- https://stackoverflow.com/a/56445634
            log (toString httpError)
            (model, Cmd.none)
        -- SearchResults results ->
        --     case results of
        --         Ok interments ->
        --             ({ model | interments = Loaded interments }, Cmd.none)
        --         Err _ ->
        --             (model, Cmd.none)
        SearchTerm searchTerm ->
            log searchTerm
            ({ model | searchTerm = searchTerm }, getSearchResults searchTerm )

view : Model -> Html Msg
view model =
    div []
        [ input [ type_ "text", class "form-control form-control-lg", placeholder "Search Term", value model.searchTerm, onInput SearchTerm ] []
        , table [ class "table", id "search_results" ] [
               thead [] [
                    tr [] [
                         th [] [ text "Family Name"]
                        , th [] [ text "Name"]
                        , th [] [ text "Death Date"]
                        , th [] [ text "Details"]
                        ]
                   ]
              , tbody [] (case model.interments of
                              Loading ->
                                  log "interments are loading"
                                  []
                              Errored errorString ->
                                  []
                              Loaded interments ->
                                  List.map resultRow interments
                              )
              ]
        ]

init : String -> (Model, Cmd Msg)
init flags =
    ( Model "" Loading
    , Cmd.none
    )

main =
    Browser.element { init = init, subscriptions = subscriptions, update = update, view = view }
