module IntermentView exposing (main)

import Browser
import Html exposing (..)

type alias Model =
    {

    }

type Msg
    = None

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        None ->
            (model, Cmd.none)

view : Model -> Html Msg
view model =
    div [] [ h1 [] [ text "Interment View Elm" ] ]

init : String -> (Model, Cmd Msg)
init flags =
    ( Model
    , Cmd.none
    )

main =
    Browser.element { init = init
                    , subscriptions = subscriptions
                    , update = update
                    , view = view }
