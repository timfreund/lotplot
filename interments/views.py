from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.contrib.postgres.search import TrigramSimilarity
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.views.generic import DetailView

from lots.models import Lot
from monuments.models import Monument
from monuments.serializers import MonumentSerializer

from .forms import IntermentForm
from .models import Interment
from .serializers import IntermentSerializer

class IntermentView(DetailView):
    template_name = "interments/interment.html"
    form_class = IntermentForm
    queryset = Interment.objects.all()

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form_class()
        i = context['interment']

        monuments = [m for m in Monument.objects.filter(interments__id=i.id).all()]
        context['monuments'] = monuments

        neighbors = [n for n in Interment.objects.filter(lot_id=i.lot_id).exclude(id=i.id).all()]
        context['neighbors'] = neighbors

        json = {
            'interment': IntermentSerializer(i, context={'request': self.request}).data,
            'monuments': MonumentSerializer(monuments, many=True, context={'request': self.request}).data,
            'neighbors': IntermentSerializer(neighbors, many=True, context={'request': self.request}).data,
        }

        context['json'] = json

        return context

class SearchByLotView(View):
    template_name = "interments/search_by_lot.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        search_string = request.POST.get("lot_search")
        payload = []
        lots = Lot.objects.filter(name__icontains=search_string).order_by('name')

        for l in lots:
            for i in l.interments.all():
                payload.append(i.json)
        return JsonResponse(payload, safe=False)

class SearchView(View):
    template_name = "interments/search.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        print(request.POST.get("interment_search"))
        search_string = request.POST.get("interment_search")
        payload = []
        results = Interment.objects.annotate(similarity=TrigramSimilarity('name', search_string)).filter(similarity__gt=0.3).order_by('-similarity')
        for i in results:
            payload.append(i.json)

        return JsonResponse(payload, safe=False)
