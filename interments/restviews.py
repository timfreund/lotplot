from django.contrib.postgres.search import TrigramSimilarity
from django_filters.rest_framework import DjangoFilterBackend, FilterSet, CharFilter
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.schemas.openapi import AutoSchema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from blobs.models import Image, Document
from blobs.restviews import ModelFieldUpload

from .models import Interment, IntermentType
from .serializers import *

__all__ = [
    'IntermentDocumentUpload',
    'IntermentImageUpload',
    'IntermentTypeViewSet',
    'IntermentViewSet',
]

class IntermentFilter(FilterSet):
    class Meta:
        model = Interment
        fields = ['family_name', 'name',]

class IntermentSchema(AutoSchema):
    def get_operation(self, path, method):
        print("get_operation %s %s" % (path, method))
        rc = super().get_operation(path, method)
        if path == '/api/interments/{id}/':
            # raise Exception()
            pass
        return rc

class IntermentViewSet(viewsets.ModelViewSet):
    queryset = Interment.objects.all()
    serializer_class = IntermentSerializer
    permission_classes = [IsAuthenticated]

    schema = IntermentSchema()

    filter_backends = [DjangoFilterBackend,]
    filterset_class = IntermentFilter

    @action(detail=False, url_path='trigram-search')
    def trigram_search(self, request):
        # TODO: how do we document the input to this method in the API?
        # TODO: when doing trigram similarity, do we actually need to
        # break the name into words and search?  Seems to fall apart
        # when the full name is long (see results for 'diffen')
        term = request.query_params.get("term", None)
        field = request.query_params.get("field", "name")
        similarity = float(request.query_params.get("similarity", 0.3))

        interments = []
        if term is not None:
            s = TrigramSimilarity(field, term)
            interments = Interment.objects.annotate(
                similarity=s).filter(
                    similarity__gt=similarity).order_by('-similarity')
        serializer = self.get_serializer(interments, many=True)
        return Response(serializer.data)

class IntermentTypeViewSet(viewsets.ModelViewSet):
    queryset = IntermentType.objects.all()
    serializer_class = IntermentTypeSerializer
    permission_classes = [IsAuthenticated]

class IntermentDocumentUpload(ModelFieldUpload):
    model_class = Interment
    model_field_name = "documents"
    blob_class = Document
    blob_field_name = "document"

class IntermentImageUpload(ModelFieldUpload):
    model_class = Interment
    model_field_name = "images"
    blob_class = Image
    blob_field_name = "full"
