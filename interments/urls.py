from django.conf.urls import url
from django.urls import path

from . import views

app_name = "interments"

urlpatterns = [
    path('interment/<int:pk>/', views.IntermentView.as_view(), name="view"),
    url(regex=r"^search$",
        view=views.SearchView.as_view(),
        name="search",
    ),
    url(regex=r"^search/bylot$",
        view=views.SearchByLotView.as_view(),
        name="search_by_lot",
    ),
]
