from django.apps import AppConfig


class IntermentsConfig(AppConfig):
    name = 'interments'
