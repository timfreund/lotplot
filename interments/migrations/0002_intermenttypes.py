from django.db import migrations, models
import django.db.models.deletion

def forward(apps, schema_editor):
    IntermentType = apps.get_model('interments', 'IntermentType')
    db = schema_editor.connection.alias
    IntermentType.objects.using(db).bulk_create([
        IntermentType(name='Burial'),
        IntermentType(name='Burial (double depth)'),
        IntermentType(name='Cremation'),
        IntermentType(name='Memorial'),
        IntermentType(name='Crypt'),
        IntermentType(name='Unknown'),
    ])

def reverse(apps, schema_editor):
    IntermentType = apps.get_model('interments', 'IntermentType')
    db = schema_editor.connection.alias
    IntermentType.objects.using(db).all().delete()

class Migration(migrations.Migration):
    dependencies = [
        ('interments', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forward, reverse),
    ]
