# Generated by Django 2.2.9 on 2020-01-29 14:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('interments', '0003_historicalinterment_historicalintermenttype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalinterment',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='death_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='disinterment_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='first_name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='interment_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='last_name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='middle_name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='historicalinterment',
            name='notes',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='interment',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='interment',
            name='death_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='interment',
            name='disinterment_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='interment',
            name='first_name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='interment',
            name='interment_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='interment',
            name='last_name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='interment',
            name='lot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='interments', to='lots.Lot'),
        ),
        migrations.AlterField(
            model_name='interment',
            name='middle_name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='interment',
            name='notes',
            field=models.TextField(blank=True),
        ),
    ]
