from django.db import models
from simple_history.models import HistoricalRecords

from blobs.models import Document, Image
from lots.models import Lot

class IntermentType(models.Model):
    name = models.CharField(unique=True, max_length=255)

    def __str__(self):
        return self.name

class Interment(models.Model):
    interment_date = models.DateField(null=True, blank=True)
    disinterment_date = models.DateField(null=True, blank=True)
    interment_type = models.ForeignKey(IntermentType, on_delete=models.PROTECT)

    lot = models.ForeignKey(Lot, on_delete=models.PROTECT, related_name='interments')

    name = models.CharField(max_length=512, blank=True)
    family_name = models.CharField(max_length=255, blank=True)

    birth_date = models.DateField(null=True, blank=True)
    death_date = models.DateField(null=True, blank=True)

    notes = models.TextField(blank=True)
    images = models.ManyToManyField(Image, blank=True)
    documents = models.ManyToManyField(Document, blank=True)

    history = HistoricalRecords()

    @property
    def age(self):
        if self.birth_date and self.death_date:
            return self.death_date - self.birth_date
        return None

    def __str__(self):
        return "%s (%s-%s)" % (self.name,
                              self.birth_date,
                              self.death_date)
