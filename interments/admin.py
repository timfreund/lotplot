from django.contrib import admin

from .models import *

admin.site.register(Interment)
admin.site.register(IntermentType)
