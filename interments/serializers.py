from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Interment, IntermentType

__all__ = [
    'IntermentSerializer',
    'IntermentTypeSerializer',
]

class IntermentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntermentType
        fields = [
            'name',
            ]

    def to_representation(self, instance):
        return instance.name

    def to_internal_value(self, data):
        it = IntermentType.objects.filter(name=data).first()
        if it is None:
            raise ValidationError("Invalid interment type")
        return it

class IntermentSerializer(serializers.HyperlinkedModelSerializer):
    interment_type = IntermentTypeSerializer()

    class Meta:
        model = Interment
        fields = [
            'id',
            'interment_date',
            'interment_type',
            'disinterment_date',
            'name',
            'family_name',
            'birth_date',
            'death_date',
            'notes',
            'lot',
            'age',
            'name',
        ]
