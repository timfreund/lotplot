import csv

from datetime import datetime, date, timedelta
from django.core.management.base import BaseCommand, CommandError

from interments.models import Interment, IntermentType
from lots.models import Lot

class ParseException(Exception):
    pass

class Command(BaseCommand):
    help = 'Import Lancaster Cemetery data from CSV'

    def add_arguments(self, parser):
        parser.add_argument('csv', type=str)

    def parse_date_string(self, date_string):
        try:
            if date_string and len(date_string):
                return datetime.strptime(date_string, "%b %d, %Y")
        except Exception as e:
            print(date_string)
            raise(ParseException(e))

    def handle(self, *args, **options):
        print(args)
        print(options)
        with open(options['csv'], 'r') as csvfile:
            csvreader = csv.DictReader(csvfile)
            rows = 0
            bad_rows = 0
            for row in csvreader:
                try:
                    rows += 1
                    birth_date = self.parse_date_string(row["Date of Birth"])
                    death_date = self.parse_date_string(row["Date of Death"])
                    interment_date = self.parse_date_string(row["Internment"])

                    lot, lot_created = Lot.objects.get_or_create(name=row["Lot"])
                    interment_type, created = IntermentType.objects.get_or_create(name="UNKNOWN")

                    interment = Interment()
                    interment.lot = lot
                    interment.interment_type = interment_type
                    interment.interment_date = interment_date
                    interment.birth_date = birth_date
                    interment.death_date = death_date
                    interment.last_name = row["Last Name"]
                    interment.first_name = row["First Name"]
                    interment.notes = row["Comments"]
                    # TODO: also grab age and funeral director

                    interment.save()
                except ParseException:
                    bad_rows += 1
                    print("BAD: %s" % row)
                    # print(row)
            print("Rows: %d" % rows)
            print("Bad rows: %d" % bad_rows)


        # for poll_id in options['poll_id']:
        #     try:
        #         poll = Poll.objects.get(pk=poll_id)
        #     except Poll.DoesNotExist:
        #         raise CommandError('Poll "%s" does not exist' % poll_id)

        #     poll.opened = False
        #     poll.save()

        #     self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
