from django.forms import ModelForm

from .models import Interment

class IntermentForm(ModelForm):
    class Meta:
        model = Interment
        fields = '__all__'
