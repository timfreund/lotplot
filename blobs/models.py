from django.db import models

class Image(models.Model):
    full = models.ImageField(max_length=255)
    thumb = models.ImageField(max_length=255)
    caption = models.TextField(null=True, blank=True)
    capture_time = models.DateTimeField(null=True, blank=True)
    upload_time = models.DateTimeField(null=True, blank=True)

class Document(models.Model):
    document = models.FileField(max_length=255)
    title = models.CharField(max_length=512, blank=True)
    notes = models.TextField(null=True, blank=True)
    upload_time = models.DateTimeField(null=True, blank=True)
