from django.core.files.storage import DefaultStorage
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

class ModelFieldUpload(APIView):
    model_class = None
    model_field_name = None
    blob_class = None
    blob_field_name = None

    parser_classes = [MultiPartParser]
    permission_classes = [IsAuthenticated]

    def post(self, request, id):
        obj = self.model_class.objects.filter(id=id).first()
        if obj is None:
            return Response(data="Object %d no found" % id,
                            status=404, exception=True)

        uploaded = request.data['file']
        b = self.blob_class()
        b_attr = getattr(b, self.blob_field_name)
        target_path = self.get_upload_path(id, uploaded.name)

        storage = DefaultStorage()
        if storage.exists(target_path):
            return Response(data="File %s exists" % target_path,
                            status=500, exception=True)

        b_attr.save(target_path, uploaded)
        getattr(obj, self.model_field_name).add(b)
        obj.save()
        return Response("%d" % b.id)

    def get_upload_path(self, model_id, file_name):
        mn = self.model_class._meta.model_name
        return "%s/%d/%s/%s" % (mn, model_id, self.model_field_name, file_name)
