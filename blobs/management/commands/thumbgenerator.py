import PIL

from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand, CommandError
from io import BytesIO

from blobs.models import Image

class Command(BaseCommand):
    def handle(self, *args, **options):
        for io in Image.objects.filter(thumb=''):
            print(io.full)
            image = PIL.Image.open(io.full.open())
            image.thumbnail(size=(200, 200))

            path, ext = io.full.name.rsplit('.', 1)
            path = "%s-thumb" % path
            full_thumb_path = ".".join((path, ext))

            bio = BytesIO()
            image.save(bio, format="jpeg")
            io.thumb.save(full_thumb_path, ContentFile(bio.getvalue()))
