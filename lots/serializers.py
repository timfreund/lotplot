from rest_framework import serializers

from .models import Lot, Map, MapCell

__all__ = [
    'LotSerializer',
    'MapSerializer',
    'MapCellSerializer',
]

class LotSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lot
        fields = [
            'id',
            'name',
        ]

class MapSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Map
        fields = [
            'id',
            'name',
        ]

class MapCellSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MapCell
        fields = [
            'id',
            'cell_id',
            'lot_name',
        ]

    def create(self, validated_data):
        cell = super().create(validated_data)
        lot = Lot.objects.filter(name=cell.lot_name).first()
        if lot:
            cell.lot = lot
            cell.save()
        return cell

    def update(self, instance, validated_data):
        cell = super().update(instance, validated_data)
        lot = Lot.objects.filter(name=cell.lot_name).first()
        if lot:
            cell.lot = lot
            cell.save()
        return cell
