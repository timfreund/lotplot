# Generated by Django 2.2.9 on 2020-03-22 01:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lots', '0002_historicallot'),
    ]

    operations = [
        migrations.CreateModel(
            name='Map',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('svg', models.FileField(max_length=255, upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='MapCell',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cell_id', models.CharField(max_length=64, unique=True)),
                ('lot_name', models.CharField(max_length=255, unique=True)),
                ('lot', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='lots.Lot')),
            ],
        ),
    ]
