from django.conf.urls import url
from django.urls import path

from . import views

app_name = "lots"

urlpatterns = [
    path('map/<int:pk>/', views.MapView.as_view(), name="view"),
]
