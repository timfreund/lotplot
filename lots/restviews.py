from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Lot, Map, MapCell
from .serializers import *

__all__ = [
    'LotViewSet',
    'MapViewSet',
    'MapCellViewSet',
]

class LotViewSet(viewsets.ModelViewSet):
    queryset = Lot.objects.all()
    serializer_class = LotSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=False, url_path='by-name')
    def by_name(self, request, pk=None):
        name = request.query_params.get("name", None)
        get_or_create = request.query_params.get("get_or_create", False)

        if name is None:
            return Response(status=500)

        lot = None
        if get_or_create:
            lot, lot_created = Lot.objects.get_or_create(name=name)
        else:
            lot = Lot.objects.filter(name=name).first()

        if lot is None:
            return Response(status=404)

        serializer = self.get_serializer(lot, many=False)
        return Response(serializer.data)

class MapViewSet(viewsets.ModelViewSet):
    queryset = Map.objects.all()
    serializer_class = MapSerializer
    permission_classes = [IsAuthenticated]

class MapCellViewSet(viewsets.ModelViewSet):
    queryset = MapCell.objects.all()
    serializer_class = MapCellSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=False, url_path='by-lot')
    def by_lot(self, request, lot=None):
        lot_name = request.query_params.get("lot_name", None)
        cells = MapCell.objects.filter(lot_name=lot_name).all()
        serializer = self.get_serializer(cells, many=True)
        return Response(serializer.data)
