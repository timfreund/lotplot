from django.db import models
from simple_history.models import HistoricalRecords

__all__ = [
    'Lot',
    'Map',
    'MapCell',
]

class Lot(models.Model):
    name = models.CharField(unique=True, max_length=255)
    history = HistoricalRecords()

    def __str__(self):
        return self.name

class Map(models.Model):
    name = models.CharField(unique=True, max_length=255)
    svg = models.FileField(max_length=255)

class MapCell(models.Model):
    cell_id = models.CharField(unique=True, max_length=64)
    lot_name = models.CharField(max_length=255)
    lot = models.ForeignKey(Lot, blank=True, null=True, on_delete=models.PROTECT)
