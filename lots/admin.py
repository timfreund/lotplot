from django.contrib import admin

from .models import *

admin.site.register(Lot)
admin.site.register(Map)
admin.site.register(MapCell)
