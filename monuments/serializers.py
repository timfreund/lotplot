from rest_framework import serializers

from .models import Material, Monument, MonumentType

__all__ = [
    'MonumentSerializer',
]

class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = [
            'name',
        ]

    def to_representation(self, instance):
        return instance.name

    def to_internal_value(self, data):
        m = Material.objects.filter(name=data).first()
        if m is None:
            raise ValidationError("Invalid material")
        return m

class MonumentSerializer(serializers.HyperlinkedModelSerializer):
    materials = MaterialSerializer(many=True)
    monument_type = serializers.StringRelatedField()

    class Meta:
        model = Monument
        fields = [
            'id',
            'name',
            'inscription',
            'materials',
            'monument_type',
            'notes',
            'interments',
            'lots',
        ]

    def create(self, validated_data):
        materials = validated_data["materials"]
        del validated_data["materials"]
        instance = Monument.objects.create(**validated_data)
        instance.materials.set(materials)
        instance.save()
        return instance
