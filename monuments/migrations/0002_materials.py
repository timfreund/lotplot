from django.db import migrations, models
import django.db.models.deletion

def forward(apps, schema_editor):
    Material = apps.get_model('monuments', 'Material')
    db = schema_editor.connection.alias
    Material.objects.using(db).bulk_create([
        Material(name='Brick'),
        Material(name='Bronze'),
        Material(name='Concrete'),
        Material(name='Granite'),
        Material(name='Marble'),
        Material(name='Wood'),
    ])

def reverse(apps, schema_editor):
    IntermentType = apps.get_model('materials', 'IntermentType')
    db = schema_editor.connection.alias
    IntermentType.objects.using(db).all().delete()

class Migration(migrations.Migration):
    dependencies = [
        ('monuments', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forward, reverse),
    ]
