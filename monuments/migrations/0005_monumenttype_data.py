from django.db import migrations, models

def forward(apps, schema_editor):
    MonumentType = apps.get_model('monuments', 'MonumentType')
    db = schema_editor.connection.alias
    MonumentType.objects.using(db).bulk_create([
        MonumentType(name='Tablet'),
        MonumentType(name='Tablet with Base'),
        MonumentType(name='Column'),
        MonumentType(name='Column (Broken)'),
        MonumentType(name='Obelisk'),
        MonumentType(name='Flat Marker'),
        MonumentType(name='Other'),
        ])

def reverse(apps, schema_editor):
    MonumentType = apps.get_model('monuments', 'MonumentType')
    db = schema_editor.connection.alias
    MonumentType.objects.using(db).all().delete()

class Migration(migrations.Migration):
    dependencies = [
        ('monuments', '0004_auto_20200211_0443'),
    ]

    operations = [
        migrations.RunPython(forward, reverse),
    ]
