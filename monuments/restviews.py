from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated

from blobs.models import Image, Document
from blobs.restviews import ModelFieldUpload

from .models import *
from .serializers import *

__all__ = [
    'MonumentDocumentUpload',
    'MonumentImageUpload',
    'MonumentViewSet',
    ]

class MonumentViewSet(viewsets.ModelViewSet):
    queryset = Monument.objects.all()
    serializer_class = MonumentSerializer
    permission_classes = [IsAuthenticated]

class MonumentDocumentUpload(ModelFieldUpload):
    model_class = Monument
    model_field_name = "documents"
    blob_class = Document
    blob_field_name = "document"

class MonumentImageUpload(ModelFieldUpload):
    model_class = Monument
    model_field_name = "images"
    blob_class = Image
    blob_field_name = "full"
