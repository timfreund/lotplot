from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from .forms import MonumentForm
from .models import Monument

class MonumentView(DetailView):
    template_name = "monuments/monument.html"
    form_class = MonumentForm
    queryset = Monument.objects.all()

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form_class()
        m = context['monument']

        return context
