from django.conf.urls import url
from django.urls import path

from . import views

app_name = "monuments"

urlpatterns = [
    path('monument/<int:pk>/', views.MonumentView.as_view(), name="view"),
]
