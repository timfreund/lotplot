from django.db import models
from simple_history.models import HistoricalRecords

from blobs.models import Document, Image
from interments.models import Interment
from lots.models import Lot

__all__ = [
    'Material',
    'Monument',
    'MonumentType',
]

class Material(models.Model):
    name = models.CharField(unique=True, max_length=255)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class MonumentType(models.Model):
    name = models.CharField(unique=True, max_length=255)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Monument(models.Model):
    name = models.CharField(max_length=255, blank=True)
    inscription = models.TextField(blank=True)
    materials = models.ManyToManyField(Material, blank=True)
    monument_type = models.ForeignKey(MonumentType, blank=True, null=True, on_delete=models.PROTECT)
    notes = models.TextField(blank=True)

    interments = models.ManyToManyField(Interment, blank=True, related_name='monuments')
    lots = models.ManyToManyField(Lot, blank=True, related_name='monuments')
    images = models.ManyToManyField(Image, blank=True)
    documents = models.ManyToManyField(Document, blank=True)

    history = HistoricalRecords()

    def __str__(self):
        return "%s (%d)" % (self.name, self.id)
