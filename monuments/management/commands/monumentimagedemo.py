from django.core.management.base import BaseCommand, CommandError

from blobs.models import Image
from interments.models import Interment
from monuments.models import Monument

class Command(BaseCommand):
    help = "Upload a monument photo"

    def add_arguments(self, parser):
        parser.add_argument('interment', type=int)
        parser.add_argument('image', type=str)

    def handle(self, *args, **options):
        print(options)

        with open(options['image'], 'rb') as imagefile:
            i = Interment.objects.filter(id=options['interment']).first()
            m = Monument.objects.filter(interments__id=options['interment']).first()
            if m is None:
                m = Monument()
                m.interments.add(i)
                m.save()

            image = Image()
            # TODO don't allow image overwrite
            image.full.save("monuments/%d/%s" % (m.id,
                                                 options['image']),
                            imagefile,
                            save=True
            )
            m.images.add(image)
            m.save()
