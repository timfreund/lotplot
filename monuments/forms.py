from django.forms import ModelForm

from .models import Monument

class MonumentForm(ModelForm):
    class Meta:
        model = Monument
        fields = '__all__'
