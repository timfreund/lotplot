#!/usr/bin/env bash

SRC=./lotplot/static/js/elm

inotifywait -q -m -e modify \
            --exclude ${SRC}/.*js \
            --format "%w%f" ${SRC} \
    | while read FILENAME
do
    echo ${FILENAME}
    elm make ${FILENAME} --output=${FILENAME}.js
done
